
Well dang, they released an iOS version: https://apps.apple.com/us/app/nfc-e-tag/id1518982217


# nfc-test-app

This is forked from [nfc-test-app](https://github.com/whitedogg13/nfc-test-app), which was referenced in the [react-native-nfc-manager](https://github.com/whitedogg13/react-native-nfc-manager#demo-project) readme. It incorporates significant code from my waveshare epaper writer, [wne_writer](https://gitlab.com/bettse/wne_writer/).

# Setup

Here are the rough steps to get this building, based on the setup section of react-native-nfc-manager

- `yarn`
- `cd ios && pod install && cd ..`
- `open ios/NfcTestApp.xcworkspace/`
- Xcode will start complaining about your provisioning profile, fix it.
