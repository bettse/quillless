import React, {Fragment, useState} from 'react';
import {
  Button,
  PixelRatio,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Switch,
  Text,
  TextInput,
  View,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import NfcManager, {NfcTech} from 'react-native-nfc-manager';
import Promise from 'bluebird';
import Canvas from 'react-native-canvas';
const {round, sqrt, pow} = Math;

const TAG_ID = 'WSDZ10m';
const TAG_ID_HEX = '5753445A31306D';

const SCREEN = {
  WIDTH: 400,
  HEIGHT: 300,
};

const ratio = PixelRatio.get();

const fonts = [
  '12px serif',
  'bold 12px serif',
  '16px serif',
  'bold 16px serif',
  '18px serif',
  'bold 18px serif',
  '24px serif',
  'bold 24px serif',
  '32px serif',
  'bold 32px serif',
  '48px serif',
  'bold 48px serif',
  '64px serif',
  'bold 64px serif',
];

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text:
        'Eating, and hospitality in general, is a communion, and any meal worth attending by yourself is improved by the multiples of those with whom it is shared.',
      fill: true,
      fontIndex: fonts.length,
      center: true,
      canvas: null,
      progress: 0,
    };
  }

  componentDidMount() {
    NfcManager.start();
  }

  componentWillUnmount() {
    this._cleanUp();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.text !== this.state.text ||
      prevState.center != this.state.center ||
      prevState.fill != this.state.fill
    ) {
      this.updateCanvas();
    }
  }

  handleCanvas = async canvas => {
    if (canvas) {
      canvas.width = SCREEN.WIDTH;
      canvas.height = SCREEN.HEIGHT;
      const ctx = await canvas.getContext('2d', {alpha: false});
      ctx.scale(1 / ratio, 1 / ratio);
      this.setState({canvas}, this.updateCanvas.bind(this));
    }
  };

  textUpdate = async text => {
    this.setState({text});
  };

  updateCanvas = async () => {
    const {canvas, center, text, fill} = this.state;
    if (canvas) {
      try {
        const ctx = await canvas.getContext('2d', {alpha: false});

        // Clear canvas
        await ctx.clearRect(0, 0, SCREEN.WIDTH, SCREEN.HEIGHT);

        ctx.textBaseline = 'top'; // makes x,y the top left
        if (center) {
          ctx.textAlign = 'center'; // makes x,y the center of the text
        } else {
          ctx.textAlign = 'start';
        }

        // Break long lines to fit on screen
        const words = text.replace('\n', ' ').split(' ');
        let lines = [];
        let width;
        let emHeightDescent;
        let start = 0;
        let count = 0;
        let fontIndex = fonts.length - 1;

        do {
          ctx.font = fonts[fontIndex--];
          lines = [];
          start = 0;
          count = 0;
          do {
            count = 0;
            do {
              count++;
              const test = words.slice(start, start + count).join(' ');
              const textMetrics = await ctx.measureText(test);
              width = textMetrics.width;
              emHeightDescent = textMetrics.emHeightDescent;
            } while (start + count - 1 < words.length && width < SCREEN.WIDTH);
            // console.log({start, count});
            // test was one word too long, so we take one less
            const line = words.slice(start, start + count - 1).join(' ');
            lines.push(line);
            start = start + count - 1;
          } while (fontIndex > 0 && start + 1 < words.length);
          /*
          console.log({
            lines,
            font: fonts[fontIndex],
            height: lines.length * emHeightDescent,
          });
          */
        } while (lines.length * emHeightDescent > SCREEN.HEIGHT);
        this.setState({fontIndex});

        // Draw onto screen
        await Promise.each(lines, async (line, i) => {
          const {emHeightDescent} = await ctx.measureText(line);
          const x = center ? SCREEN.WIDTH / 2 : 0;

          if (fill) {
            await ctx.fillText(line, x, i * emHeightDescent, SCREEN.WIDTH);
          } else {
            await ctx.strokeText(line, x, i * emHeightDescent, SCREEN.WIDTH);
          }
        });
      } catch (err) {
        console.log('Error updating canvas', err);
      }
    }
  };

  render() {
    const {center, fill, text, progress, fontIndex} = this.state;
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView style={styles.container}>
          <View style={styles.body}>
            <View style={styles.sectionContainer}>
              <Text style={styles.sectionTitle}>Quillless</Text>
              <Text style={styles.sectionDescription}>
                {progress > 0 && `${progress.toFixed(2)}%\n`}
                Shake for development options
              </Text>
            </View>
            <View style={styles.sectionContainer}>
              <TextInput
                multiline={true}
                autoCompleteType="off"
                autoCorrect={false}
                autoFocus={true}
                defaultValue={text}
                onChangeText={this.textUpdate.bind(this)}
              />
              <Text style={styles.sectionDescription}>
                Autofit font: {fonts[fontIndex]}
              </Text>
              <Text>Center: </Text>
              <Switch
                trackColor={{false: '#767577', true: '#81b0ff'}}
                thumbColor={center ? '#f5dd4b' : '#f4f3f4'}
                ios_backgroundColor="#3e3e3e"
                onValueChange={center => this.setState({center})}
                value={center}
              />
              <Text>Fill: </Text>
              <Switch
                trackColor={{false: '#767577', true: '#81b0ff'}}
                thumbColor={fill ? '#f5dd4b' : '#f4f3f4'}
                ios_backgroundColor="#3e3e3e"
                onValueChange={fill => this.setState({fill})}
                value={fill}
              />
            </View>
            <View style={styles.sectionContainer}>
              <Canvas
                ref={this.handleCanvas}
                id="canvas"
                style={styles.canvas}
              />
            </View>
            <View style={styles.sectionContainer}>
              <Button title="Write" onPress={this._test} />
            </View>
          </View>
        </SafeAreaView>
      </Fragment>
    );
  }

  _cleanUp = () => {
    NfcManager.cancelTechnologyRequest().catch(() => 0);
  };

  transceive = async c => {
    const command = Array.from(c);
    try {
      return await NfcManager.sendMifareCommandIOS(command);
    } catch (err) {
      console.log('error sending command', command, err);
      throw err;
    }
  };

  tagSetup = async () => {
    const commands = [
      [0xcd, 0xd],
      [0xcd, 0x0, 0xa],
      [0xcd, 0x1],
      [0xcd, 0x2],
      [0xcd, 0x3],
      [0xcd, 0x5],
      [0xcd, 0x6],
      [0xcd, 0x7, 0x0],
    ];
    try {
      await Promise.each(commands, async command => {
        await Promise.delay(100);
        await this.transceive(command);
      });
    } catch (err) {
      console.log('error during setup', err);
      throw err;
    }
  };

  tagFinal = async () => {
    const commands = [
      // [0xcd, 0x18], // epaper power on
      [0xcd, 0x9], // refresh
      [0xcd, 0xa], // wait
      [0xcd, 0x4], // epaper power off
    ];

    try {
      await Promise.each(commands, async command => {
        await this.transceive(command);
        await Promise.delay(200);
      });
    } catch (err) {
      console.log('error during final', err);
      throw err;
    }
  };

  getPbm = async () => {
    const {canvas} = this.state;
    try {
      const ctx = await canvas.getContext('2d', {alpha: false});
      const imageData = await ctx.getImageData(
        0,
        0,
        SCREEN.WIDTH,
        SCREEN.HEIGHT,
      );
      const rgba = Object.values(imageData.data);
      // console.log({rgba: rgba.filter(x => x > 0)});
      // Average the image data to make it black and white
      const bw = Array(rgba.length / 4);
      var i = 0;
      for (var i = 0; i < bw.length; i++) {
        const [r, g, b, a] = rgba.slice(i * 4, i * 4 + 4);
        const distance = sqrt(pow(r, 2) + pow(g, 2) + pow(b, 2) + pow(a, 2));
        bw[i] = distance > 180 ? 1 : 0;
      }
      // console.log({bw: bw.filter(x => x > 0)});

      // Back black/white data into bytes
      const pbm = Array(bw.length / 8);
      for (var i = 0; i < pbm.length; i++) {
        const bits = bw.slice(i * 8, i * 8 + 8);
        let byte = 0;
        byte |= bits[0] << 7;
        byte |= bits[1] << 6;
        byte |= bits[2] << 5;
        byte |= bits[3] << 4;
        byte |= bits[4] << 3;
        byte |= bits[5] << 2;
        byte |= bits[6] << 1;
        byte |= bits[7] << 0;
        pbm[i] = byte;
      }
      // console.log(JSON.stringify({pbm}));
      return pbm;
    } catch (err) {
      console.log('error converting canvas to b/w data', err);
      throw err;
    }
  };

  _test = async () => {
    try {
      const pbm = await this.getPbm();

      let resp = await NfcManager.requestTechnology(NfcTech.MifareIOS, {
        alertMessage: 'HOLD STEADY!',
      });

      const {id} = await NfcManager.getTag();
      if (id !== TAG_ID_HEX) {
        console.warn(`INCORRECT UID ${id}`);
        throw new Error('INCORRECT UID');
      }

      await this.tagSetup();

      const line_prefix = [0xcd, 0x08, 0x64];
      const line_width = 100;
      for (var i = 0; i < 150; i++) {
        // const line = Array(line_width); line.fill(0);
        const line = pbm.slice(i * line_width, i * line_width + line_width);
        await NfcManager.sendMifareCommandIOS(line_prefix.concat(line));
        await Promise.delay(20);
        this.setState({progress: (i / 150.0) * 100});
      }

      await this.tagFinal();
      this.setState({progress: 0});

      this._cleanUp();
    } catch (ex) {
      console.warn('ex', ex);
      this._cleanUp();
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  canvas: {
    borderStyle: 'solid',
    borderColor: 'black',
    width: SCREEN.WIDTH / ratio,
    height: SCREEN.HEIGHT / ratio,
    borderWidth: 1,
  },
  button: {
    padding: 10,
    width: 100,
    borderWidth: 1,
    borderColor: 'black',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
